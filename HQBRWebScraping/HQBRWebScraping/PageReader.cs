﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Linq;
using System;

namespace HqBrWebScraping
{
    public class WebScraper
    {
        public void Read()
        {
            //https://stackoverflow.com/questions/18156795/parsing-html-to-get-script-variable-value

            using (WebClient client = new WebClient())
            {
                string resource = client.DownloadString("https://hqbr.com.br/hqs/The%20Walking%20Dead/capitulo/2/leitor/0#1/");
                HtmlAgilityPack.HtmlDocument html = new HtmlAgilityPack.HtmlDocument();
                html.LoadHtml(resource);

                var script = ReadHtmlDocument(html);

                var links = ParseScriptTag(script);

                var preT = "https://hqbr.com.br";
                var t = links.ElementAt(1);
                client.DownloadFile(new Uri(preT+t), @"c:\temp\image35.png");
            }


        }
        private string ReadHtmlDocument(HtmlAgilityPack.HtmlDocument html)
        {
            var scriptList = html.DocumentNode.Descendants()
                                             .Where(n => n.Name == "script")
                                             .ToList();
            return GetPagesScript(scriptList);
        }

        private string GetPagesScript(List<HtmlAgilityPack.HtmlNode> scriptList)
        {
            var script = scriptList.ElementAt(4).InnerText;
            return script;
        }

        private List<string> ParseScriptTag(string script)
        {
            string[] lines = script.Replace("\r", "").Split('\n');
            string line = lines[2];

            Regex regex = new Regex(@"\[(.*?)\]");
            Match match = regex.Match(line);

            if (match.Success)
            {
                string parseLine = match.Value.Replace("[", "").Replace("]", "").Replace("\"","");
                string[] linksArray = parseLine.Split(',');
                List<string> LinksList = linksArray.OfType<string>().ToList(); // this isn't going to be fast.
                return LinksList;
            }
            return null;
        }

    }
}
